# Boilerplate

Custom Next.js boilerplate for WCaaS projects

## Install

install: 
```npm i```

development: 
```npm start```

build: 
```npm run build```

## Development

##### Node version
node 8.11.3+
npm 6.1.0+
