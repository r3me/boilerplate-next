import React from 'react'

import Meta from '../components/assets/meta'
import Layout from '../components/layout/layout'

export default () => (
  <Layout>
    <Meta title="Landing Page" />
    <p>Landing page</p>
  </Layout>
)
