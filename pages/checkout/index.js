import React from 'react'

import Meta from '../../components/assets/meta'
import Layout from '../../components/layout/layout'

export default () => (
  <Layout>
    <Meta title="Checkout" />
    <p>Checkout page</p>
  </Layout>
)
