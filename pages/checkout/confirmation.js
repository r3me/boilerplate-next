import React from 'react'

import Layout from '../../components/layout/layout'

export default () => (
  <Layout>
    <p>Confirmation page</p>
  </Layout>
)
