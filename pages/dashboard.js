import React from 'react'

import Meta from '../../components/assets/meta'
import Layout from '../../components/layout/layout'

export default () => (
  <Layout>
    <main>
      <Meta title="Dashboard" />
      <p>Dashboard</p>
    </main>
  </Layout>
)
