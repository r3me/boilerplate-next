import React from 'react'

import Meta from '../components/assets/meta'
import Layout from '../components/layout/layout'

export default () => (
  <Layout>
    <Meta title="Producto" />
    <p>Detalle de producto</p>
  </Layout>
)
