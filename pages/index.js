import React from 'react'

import Meta from '../components/assets/meta'
import Layout from '../components/layout/layout'

const Index = () => (
  <Layout>
    <main>
      <Meta title="Home" />
      <i />
      <p>Hello Next</p>
    </main>
  </Layout>
)

export default Index
