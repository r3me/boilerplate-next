import React from 'react'
import Head from 'next/head'

export default (props) => (
  <Head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>{props.title}</title>
  </Head>
)
