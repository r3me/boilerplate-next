import React from 'react';

import Header from './header'

const DefaultLayout = ({children}) => (
  <div className="layout layout--default">
    <Header />
    {children}
  </div>
)

export default DefaultLayout
