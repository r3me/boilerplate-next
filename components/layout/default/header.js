import React from 'react'
import Link from 'next/link'

const Header = () => (
  <header>
    <h1>Default Layout</h1>
    <nav>
      <Link href="/">
        <a>Home</a>
      </Link>
      <Link href="/catalog">
        <a>Catálogo</a>
      </Link>
      <Link href="/product">
        <a>Producto</a>
      </Link>
      <Link href="/cart">
        <a>Carrito</a>
      </Link>
      <Link href="/checkout">
        <a>Checkout</a>
      </Link>
      <Link href="/landing">
        <a>Landing</a>
      </Link>
    </nav>
  </header>
)

export default Header
