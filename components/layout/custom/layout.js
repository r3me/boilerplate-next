import React from 'react';

import Header from './header'

const CustomLayout = ({ children }) => (
  <div className="layout layout--custom">
    <Header />
    {children}
  </div>
)

export default CustomLayout
