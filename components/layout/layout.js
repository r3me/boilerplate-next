import React from 'react';
import { withRouter } from 'next/router'
import '../../sass/index.scss'

import DefaultLayout from '../layout/default/layout'
import CheckoutLayout from '../layout/checkout/layout'
import CustomLayout from '../layout/custom/layout'
import BlankLayout from '../layout/blank/layout'

const Layout = (props) => {
  const isCheckout = props.router.pathname.substring(0, 9) === '/checkout'
  const isLanding = props.router.pathname.substring(0, 8) === '/landing'
  const isBlank = props.router.pathname.substring(0, 8) === '/example'
  if (isCheckout) {
    return(
      <CheckoutLayout>
        {props.children}
      </CheckoutLayout>
    )
  } else if (isLanding) {
    return (
      <CustomLayout>
        {props.children}
      </CustomLayout>
    )
  } else if (isBlank) {
    return (
      <BlankLayout>
        {props.children}
      </BlankLayout>
    )
  } else {
    return (
      <DefaultLayout>
        {props.children}
      </DefaultLayout>
    )
  }
}

export default withRouter(Layout)
