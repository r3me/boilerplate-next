import React from 'react';

const BlankLayout = ({ children }) => (
  <div className="layout--blank">
    {children}
  </div>
)

export default BlankLayout
